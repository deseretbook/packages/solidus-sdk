# 🛍️ solidus-sdk

A JavaScript SDK for interacting with a Solidus E-commerce API

**[See the docs for more information](https://solidus-sdk.netlify.com/)**

## Installation

`yarn add solidus-sdk`

## Usage

```
import { Solidus } from 'solidus-sdk'

const sdk = new Solidus({ href: 'http://localhost:3000' })

sdk.product({ id: '1234' }).get()
  .then(product => console.log('Response from API: ', product))
```
