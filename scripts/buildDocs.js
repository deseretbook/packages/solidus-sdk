const fs = require('fs')
const { Solidus } = require('../dist/solidus-sdk.cjs.js')

const linkLabels = {
  source: 'Source',
  solidus: 'Solidus API Docs',
}

/**
 * Dynamically builds a block of documentation for the given endpoint. Parses the schema to figure
 * out how to best document its usage.
 *
 * @param {object} endpoint - Endpoint data
 * @param {array<object>} [context=[]] - Array of strings containing the parent method names.
 */
const buildEndpointBlock = (endpoint, context = []) => {
  if (!endpoint) {
    return null
  }

  if (!!endpoint.endpoints) {
    return Object.entries(endpoint.endpoints)
      .map(([key, value]) => buildEndpointBlock(value, [...context, key]))
      .join('\n\n')
  }

  const docs = endpoint.documentation || {}

  const block = []

  // Title
  if (docs.title || context.length) {
    block.push(`## ${docs.title || context[context.length - 1]}`)
  }

  // Endpoint path / method
  block.push(`\`[${endpoint.method}] ${endpoint.path}\``)

  // Extra attributes in docs.
  if (docs.attributes && docs.attributes.length) {
    block.push(`
      <p class="attributes">
        ${docs.attributes.map(attr => `<span>${attr}</span>`)}
      </p>
    `)
  }

  // Description
  block.push(
    docs.description ||
      `
    > **🚨 This endpoint is missing documentation.**
    >
    > Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
    be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
    >
    > Here are some helpful links that should help you get started:
    >
    > - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
    > - [Solidus API Reference](https://solidus.docs.stoplight.io/)
    > - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
    > - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)
  `,
  )

  // Examples
  if (docs.examples && docs.examples.length) {
    block.push('### Usage')

    docs.examples.forEach(example => {
      block.push(example.description)
      block.push(`
        \`\`\`
        import { Solidus } from 'solidus-sdk'
    
        const instance = new Solidus({ href: 'https://mysolidus.app' })
    
        instance${context
          .map(
            (name, idx) =>
              `.${name}(${(example.values && example.values[idx]) || ''})`,
          )
          .join('')}
        \`\`\`  
      `)
    })
  }

  // Useful links
  const links = Object.entries(docs.links || {}).map(
    ([type, href]) => `[${linkLabels[type] || type}](${href})`,
  )

  if (links.length) {
    block.push(`
      ### Helpful Links

      ${links.map(link => `- ${link}`).join('\n')}
    `)
  }

  return block.filter(Boolean).join('\n\n')
}

;(() => {
  const instance = new Solidus({ href: 'https://mysolidus.app' })
  const directory = './docs/api'

  // Make sure the directory exists before we write files to it.
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory)
  }

  // Go through every top level endpoint and make a new documentation file for it.
  Object.entries(instance.__endpoints).map(([endpointName, endpoint]) => {
    const path = `${directory}/${endpointName}.md`

    // Delete the existing file if one is already present.
    try {
      if (fs.existsSync(path)) {
        fs.unlinkSync(path)
      }
    } catch (error) {
      console.error('Unable to delete the docs file.', error)
    }

    const title =
      (endpoint.documentation && endpoint.documentation.title) || endpointName

    const docBlocks = [
      `
        ---
        id: ${endpointName}
        title: ${title}
        ---
      `,
      (endpoint.documentation && endpoint.documentation.description) ||
        `Allows you to interact with ${title.toLowerCase()} and related endpoints.`,
      endpoint.documentation &&
        endpoint.documentation.links &&
        endpoint.documentation.links.guide &&
        `For more information on how ${title.toLowerCase()} work, [see the Solidus Developer's Guide](${
          endpoint.documentation.links.guide
        })`,
    ]

    // Add doc blocks for every endpoint
    Object.entries(endpoint.endpoints).forEach(([name, childEndpoint]) => {
      docBlocks.push(buildEndpointBlock(childEndpoint, [endpointName, name]))
    })

    // Save the new documentation file.
    fs.writeFile(
      path,
      docBlocks
        .filter(Boolean)
        .join('\n\n') // Join blocks together with space between
        .replace(/^[^\S\r\n]{2,}/gm, '') // Remove leading spaces on each line
        .replace(/[\r\n]{3,}/g, '\n\n') // Remove excess empty lines
        .replace(/^[\r\n]+/, '') // Remove empty lines at the start of the file
        .replace(/[\r\n]+$/, ''), // Remove empty lines at the end of the file
      {},
      () => {
        console.log(path, ' built successfully.')
      },
    )
  })
})()
