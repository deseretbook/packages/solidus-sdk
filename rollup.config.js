import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import pkg from './package.json'
import babel from 'rollup-plugin-babel'
import minify from 'rollup-plugin-babel-minify'
import stripCode from 'rollup-plugin-strip-code'

const isProduction = process.env.NODE_ENV === 'production'

export default [
  // CommonJS (for Node) and ES module (for bundlers) build.
  {
    input: 'src/index.js',
    plugins: [
      resolve(),
      commonjs(),
      babel({
        exclude: 'node_modules/**',
      }),
      isProduction &&
        stripCode({
          start_comment: 'documentation start',
          end_comment: 'documentation end',
        }),
      isProduction &&
        minify({
          comments: false,
        }),
    ].filter(Boolean),
    external: Object.keys(pkg.dependencies || {}),
    output: [
      { file: pkg.main, format: 'cjs' },
      { file: pkg.module, format: 'es' },
    ],
  },
]
