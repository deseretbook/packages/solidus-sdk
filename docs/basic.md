---
id: basic
title: Basic Usage
---

Before getting started, we''ll assume you already have an existing app running with Solidus setup.
If you don't, [get setup with Solidus first](https://github.com/solidusio/solidus#getting-started).

## Installation

Using [Yarn](https://yarnpkg.com/lang/en/) you can add the `solidus-sdk` to your app.

```
yarn add solidus-sdk
```

Once you have `solidus-sdk` installed, you can get started using it like so:

``` javascript
import { Solidus } from 'solidus-sdk'

const sdk = new Solidus({ href: 'https://mysolidus.app' })
```
