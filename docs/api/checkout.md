---
id: checkout
title: checkout
---

Allows you to interact with checkout and related endpoints.

## update

`[PATCH] /api/checkouts/:number`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## next

`[PUT] /api/checkouts/:number/next`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## advance

`[PUT] /api/checkouts/:number/advance`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## complete

`[PUT] /api/checkouts/:number/complete`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/checkouts/checkout.number/line_items`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/checkouts/:checkout.number/line_items`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/checkouts/:checkout.number/line_items/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/checkouts/:checkout.number/line_items/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/checkouts/:checkout.number/line_items/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/checkouts/:checkout.number/line_items/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/checkouts/:checkout.number/line_items`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/checkouts/:checkout.number/payments`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/checkouts/:checkout.number/payments`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/checkouts/:checkout.number/payments/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/checkouts/:checkout.number/payments/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/checkouts/:checkout.number/payments/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/checkouts/:checkout.number/payments/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/checkouts/:checkout.number/payments/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## authorize

`[PUT] /api/checkouts/:checkout.number/payments/:id/authorize`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## capture

`[PUT] /api/checkouts/:checkout.number/payments/:id/capture`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## purchase

`[PUT] /api/checkouts/:checkout.number/payments/:id/purchase`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## void

`[PUT] /api/checkouts/:checkout.number/payments/:id/void`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## credit

`[PUT] /api/checkouts/:checkout.number/payments/:id/credit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/checkouts/:checkout.number/addresses/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/checkouts/:checkout.number/addresses/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/checkouts/:checkout.number/addresses/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/checkouts/:checkout.number/return_authorizations`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/checkouts/:checkout.number/return_authorizations`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/checkouts/:checkout.number/return_authorizations/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/checkouts/:checkout.number/return_authorizations/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## cancel

`[PUT] /api/checkouts/:checkout.number/return_authorizations/:id/cancel`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/checkouts/:checkout.number/return_authorizations/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/checkouts/:checkout.number/return_authorizations/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)