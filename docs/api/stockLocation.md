---
id: stockLocation
title: stockLocation
---

Allows you to interact with stocklocation and related endpoints.

## all

`[GET] /api/stock_locations`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/stock_locations`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/stock_locations/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/stock_locations/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/stock_locations/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/stock_locations/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/stock_locations/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/stock_locations/:stockLocation.id/stock_items`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/stock_locations/:stockLocation.id/stock_items`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/stock_locations/:stockLocation.id/stock_items/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/stock_locations/:stockLocation.id/stock_items/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/stock_locations/:stockLocation.id/stock_items/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/stock_locations/:stockLocation.id/stock_items/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/stock_locations/:stockLocation.id/stock_items/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/stock_locations/:stockLocation.id/stock_movements`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/stock_locations/:stockLocation.id/stock_movements`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/stock_locations/:stockLocation.id/stock_movements/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/stock_locations/:stockLocation.id/stock_movements/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/stock_locations/:stockLocation.id/stock_movements/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/stock_locations/:stockLocation.id/stock_movements/:id/update`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/stock_locations/:stockLocation.id/stock_movements/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)