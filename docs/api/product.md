---
id: product
title: Products
---

Allows you to interact with products and related endpoints.

For more information on how products work, [see the Solidus Developer's Guide](https://guides.solidus.io/developers/products-and-variants/overview.html)

## Get All Products

`[GET] /api/products`

<p class="attributes">
<span>paginated</span>
</p>

Retrieves a list of available products.

### Usage

```
import { Solidus } from 'solidus-sdk'

const instance = new Solidus({ href: 'https://mysolidus.app' })

instance.product().all()
```  

Retrieve paginated results

```
import { Solidus } from 'solidus-sdk'

const instance = new Solidus({ href: 'https://mysolidus.app' })

instance.product().all({ per_page: 50, page: 3 })
```  

Retrieve a list of products by their id (comma separated string).

```
import { Solidus } from 'solidus-sdk'

const instance = new Solidus({ href: 'https://mysolidus.app' })

instance.product().all({ ids: [1,2,3,4,5].join(',')})
```  

### Helpful Links

- [Source](https://github.com/solidusio/solidus/blob/master/api/app/controllers/spree/api/products_controller.rb#L6)
- [Solidus API Docs](https://solidus.docs.stoplight.io/api-reference/products/list-products)

## Create Product

`[POST] /api/products`

<p class="attributes">
<span>admin</span>
</p>

Creates a new product.

This should be used along side the `new()` method to learn attributes can be saved
to the product and which are required.

**Note:** The values sent to this method should be wrapped inside a `product` property.
See examples for more details.

### Usage

```
import { Solidus } from 'solidus-sdk'

const instance = new Solidus({ href: 'https://mysolidus.app' })

instance.product().create({ product })
```  

### Helpful Links

- [Source](https://github.com/solidusio/solidus/blob/master/api/app/controllers/spree/api/products_controller.rb#L73)
- [Solidus API Docs](https://solidus.docs.stoplight.io/api-reference/products/create-product)

## Learn How to Create a Product

`[GET] /api/products/new`

This endpoint returns information about what attributes are accepted by
the API and which are required when creating a new product.

You can use this endpoint to help you build a form that allows users to create new
products.

### Usage

```
import { Solidus } from 'solidus-sdk'

const instance = new Solidus({ href: 'https://mysolidus.app' })

instance.product().new()
```  

### Helpful Links

- [Source](https://github.com/solidusio/solidus/blob/master/api/app/controllers/spree/api/products_controller.rb#L29)

## Learn How to Edit a Product

`[GET] /api/products/:id/edit`

<p class="attributes">
<span>admin</span>
</p>

This endpoint returns information about what attributes are accepted by
the API and which are required when editing an existing product.

You can use this endpoint to help you build a form that allows users to edit products.

### Usage

```
import { Solidus } from 'solidus-sdk'

const instance = new Solidus({ href: 'https://mysolidus.app' })

instance.product({ id }).edit()
```  

## get

`[GET] /api/products/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/products/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[PATCH] /api/products/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/products/:product.id/images`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/products/:product.id/images`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/products/:product.id/images/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/products/:product.id/images/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/products/:product.id/images/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/products/:product.id/images/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/products/:product.id/images/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/products/:product.id/variants`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/products/:product.id/variants`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/products/:product.id/variants/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/products/:product.id/variants/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/products/:product.id/variants/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/products/:product.id/variants/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/products/:product.id/variants/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/products/:product.id/product_properties`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/products/:product.id/product_properties`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/products/:product.id/product_properties/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/products/:product.id/product_properties/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/products/:product.id/product_properties/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/products/:product.id/product_properties/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/products/:product.id/product_properties/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)