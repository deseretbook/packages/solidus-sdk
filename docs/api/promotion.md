---
id: promotion
title: Promotions
---

Allows you to interact with promotions and related endpoints.

For more information on how promotions work, [see the Solidus Developer's Guide](https://guides.solidus.io/developers/promotions/overview.html)

## Get Promotion

`[GET] /api/promotions/:id`

<p class="attributes">
<span>admin</span>
</p>

Retrieves a promotion by its `id`.

### Usage

```
import { Solidus } from 'solidus-sdk'

const instance = new Solidus({ href: 'https://mysolidus.app' })

instance.promotion().get({ id })
```  

### Helpful Links

- [Source](https://github.com/solidusio/solidus/blob/master/api/app/controllers/spree/api/promotions_controller.rb#L9)
- [Solidus API Docs](https://solidus.docs.stoplight.io/api-reference/promotions/get-promotion)