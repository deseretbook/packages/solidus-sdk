---
id: country
title: country
---

Allows you to interact with country and related endpoints.

## all

`[GET] /api/countries`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/countries/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/countries/:country.id/states`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/countries/:country.id/states/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)