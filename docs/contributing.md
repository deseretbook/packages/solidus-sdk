---
id: contributing
title: Contributing
sidebar_label: Contributing
---

There are several ways you can help improve solidus API and the solidus-sdk:

## 1. Contribute directly to Solidus API.

The better the API is, the more useful the sdk will be.

[Here are all the open issues related to the API.](https://github.com/solidusio/solidus/issues?utf8=%E2%9C%93&q=is%3Aissue+is%3Aopen+label%3AAPI)


## 2. Update Documentation

So far a lot of documentation is missing still, but is easy to add. If you're new
to open source or looking for an easy way to help out, then this is a great place
to start that will provide a lot of value.

### How to Add Documentation

Documentation is automatically generated based on extra info in the schema, the following is all the available options for adding docs to an endpoint. Every endpoint should ultimately have documentation.

#### Some things to note

1. Documentation should be added to a `documentation` property inside the endpoint schema.
2. `documentation` property _must be_ wrapped with `/* documentation start */` and
`/* documentation end */` tags. This is so we can remove docs from the published
package to keep package size down.

Here's an example:

```
{
  ...

  get: {
    method: 'GET',
    path: '/api/promotions/:id',
    /* documentation start */
    documentation: {
      ...documentationGoesHere,
    },
    /* documentation end */
  },

  ...
}
```

#### Available Options

**title [string]**

A short descriptive title of what the endpoint does.

EG: "Create Product"

**description [string]**

A full description of what the endpoint does, as well as any side effects or
other important information the caller might need to know.

This field supports any type of markdown syntax.

**attributes [array[string]]**

A list of attributes that describe the endpoint. The list can be of anything, but the
following are currently the prefered attributes that can/should be added:

- "paginated" - When an endpoint's response supports pagination
- "admin" - When an endpoint requires the user to be an admin.

**examples [array[object]]**

A list of examples to be generated for the endpoint, each object in the array will
render a new example based on the given info. The following are attributes that can
be used in each example:

- **description [string]** - Description of the example
- **values [array[string|null]]** - Array of values to pass to the example's methods.

EG:

```
{
  ...


  all: {
    method: 'GET',
    path: '/api/whatever',
    /* documentation start */
    documentation: {
      examples: [
        {
          description: 'this is the description',
          values: ['{ per_page: 50, page: 5 }'],
        }
      ]
    },
    /* documentation end */
  },

  ...
}
```

**links [object[string]]**

An object containing related and useful links. There are several types of links that
are supported and should be added when appropriate. The following are supported link
types:

- "guide" - URL to related page in the [Solidus Developers Guide](https://guides.solidus.io/developers).
- "solidus" - URL to related [Solidus API docs](https://solidus.docs.stoplight.io/).
- "source" - URL to endpoint's source in the [Solidus repo](https://github.com/solidusio/solidus).


## 3. Contribute to solidus-sdk

Report any issues you find, open merge requests for any endpoints that are missing, etcetera. We're always looking for more help!

## 4. Write a Plugin

There are lots of extensions to Solidus, and many of them have API support as well.
Each one of these needs a related plugin package. [Check out the Plugins docs for further information](plugins.md).
