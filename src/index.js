import { createSdk } from 'sdk-factory'

export const Solidus = createSdk({
  name: 'Solidus',
  headers: {
    'Content-Type': 'application/json',
  },
  requestResolver: {
    resolveHeaders({ settings, context, params }) {
      return headers => {
        const token =
          (context.order && context.order.token) ||
          (context.checkout && context.checkout.token) ||
          (params.order && params.order.token) ||
          params.token ||
          params.order_token

        return {
          ...(headers || settings.headers || {}),
          ...(!!settings.apiKey && {
            Authorization: `Bearer ${settings.apiKey}`,
          }),
          ...(!!token && { 'X-Spree-Order-Token': token }),
        }
      }
    },
  },
  endpoints: {
    promotion: {
      /* documentation start */
      documentation: {
        title: 'Promotions',
        links: {
          guide:
            'https://guides.solidus.io/developers/promotions/overview.html',
        },
      },
      /* documentation end */
      endpoints: {
        get: {
          method: 'GET',
          path: '/api/promotions/:id',
          /* documentation start */
          documentation: {
            title: 'Get Promotion',
            description: 'Retrieves a promotion by its `id`.',
            examples: [
              {
                values: [null, '{ id }'],
              },
            ],
            links: {
              source:
                'https://github.com/solidusio/solidus/blob/master/api/app/controllers/spree/api/promotions_controller.rb#L9',
              solidus:
                'https://solidus.docs.stoplight.io/api-reference/promotions/get-promotion',
            },
            attributes: ['admin'],
          },
          /* documentation end */
        },
      },
    },

    product: {
      /* documentation start */
      documentation: {
        title: 'Products',
        links: {
          guide:
            'https://guides.solidus.io/developers/products-and-variants/overview.html',
        },
      },
      /* documentation end */
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/products',
          /* documentation start */
          documentation: {
            title: 'Get All Products',
            description: `Retrieves a list of available products.`,
            attributes: ['paginated'],
            examples: [
              {
                values: [null, null],
              },
              {
                description: 'Retrieve paginated results',
                values: [null, '{ per_page: 50, page: 3 }'],
              },
              {
                description:
                  'Retrieve a list of products by their id (comma separated string).',
                values: [null, "{ ids: [1,2,3,4,5].join(',')}"],
              },
            ],
            links: {
              source:
                'https://github.com/solidusio/solidus/blob/master/api/app/controllers/spree/api/products_controller.rb#L6',
              solidus:
                'https://solidus.docs.stoplight.io/api-reference/products/list-products',
            },
          },
          /* documentation end */
        },
        create: {
          method: 'POST',
          path: '/api/products',
          /* documentation start */
          documentation: {
            title: 'Create Product',
            description: `
              Creates a new product.

              This should be used along side the \`new()\` method to learn attributes can be saved
              to the product and which are required.

              **Note:** The values sent to this method should be wrapped inside a \`product\` property.
              See examples for more details.
            `,
            attributes: ['admin'],
            examples: [
              {
                values: [null, '{ product }'],
              },
            ],
            links: {
              source:
                'https://github.com/solidusio/solidus/blob/master/api/app/controllers/spree/api/products_controller.rb#L73',
              solidus:
                'https://solidus.docs.stoplight.io/api-reference/products/create-product',
            },
          },
          /* documentation end */
        },
        new: {
          method: 'GET',
          path: '/api/products/new',
          /* documentation start */
          documentation: {
            title: 'Learn How to Create a Product',
            description: `
              This endpoint returns information about what attributes are accepted by
              the API and which are required when creating a new product.

              You can use this endpoint to help you build a form that allows users to create new
              products.
            `,
            examples: [
              {
                values: [null, null],
              },
            ],
            links: {
              source:
                'https://github.com/solidusio/solidus/blob/master/api/app/controllers/spree/api/products_controller.rb#L29',
            },
          },
          /* documentation end */
        },
        edit: {
          method: 'GET',
          path: '/api/products/:id/edit',
          /* documentation start */
          documentation: {
            title: 'Learn How to Edit a Product',
            description: `
              This endpoint returns information about what attributes are accepted by
              the API and which are required when editing an existing product.

              You can use this endpoint to help you build a form that allows users to edit products.
            `,
            attributes: ['admin'],
            examples: [
              {
                values: ['{ id }', null],
              },
            ],
          },
          /* documentation end */
        },
        get: {
          method: 'GET',
          path: '/api/products/:id',
        },
        update: {
          method: 'PATCH',
          path: '/api/products/:id',
        },
        delete: {
          method: 'PATCH',
          path: '/api/products/:id',
        },
        image: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/products/:product.id/images',
            },
            create: {
              method: 'POST',
              path: '/api/products/:product.id/images',
            },
            new: {
              method: 'GET',
              path: '/api/products/:product.id/images/new',
            },
            edit: {
              method: 'GET',
              path: '/api/products/:product.id/images/:id/edit',
            },
            get: {
              method: 'GET',
              path: '/api/products/:product.id/images/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/products/:product.id/images/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/products/:product.id/images/:id',
            },
          },
        },
        variant: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/products/:product.id/variants',
            },
            create: {
              method: 'POST',
              path: '/api/products/:product.id/variants',
            },
            new: {
              method: 'GET',
              path: '/api/products/:product.id/variants/new',
            },
            edit: {
              method: 'GET',
              path: '/api/products/:product.id/variants/:id/edit',
            },
            get: {
              method: 'GET',
              path: '/api/products/:product.id/variants/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/products/:product.id/variants/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/products/:product.id/variants/:id',
            },
          },
        },
        property: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/products/:product.id/product_properties',
            },
            create: {
              method: 'POST',
              path: '/api/products/:product.id/product_properties',
            },
            new: {
              method: 'GET',
              path: '/api/products/:product.id/product_properties/new',
            },
            edit: {
              method: 'GET',
              path: '/api/products/:product.id/product_properties/:id/edit',
            },
            get: {
              method: 'GET',
              path: '/api/products/:product.id/product_properties/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/products/:product.id/product_properties/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/products/:product.id/product_properties/:id',
            },
          },
        },
      },
    },

    checkout: {
      endpoints: {
        update: {
          method: 'PATCH',
          path: '/api/checkouts/:number',
        },
        next: {
          method: 'PUT',
          path: '/api/checkouts/:number/next',
        },
        advance: {
          method: 'PUT',
          path: '/api/checkouts/:number/advance',
        },
        complete: {
          method: 'PUT',
          path: '/api/checkouts/:number/complete',
        },
        lineItem: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/checkouts/checkout.number/line_items',
            },
            create: {
              method: 'POST',
              path: '/api/checkouts/:checkout.number/line_items',
            },
            new: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/line_items/new',
            },
            edit: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/line_items/:id/edit',
            },
            get: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/line_items/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/checkouts/:checkout.number/line_items/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/checkouts/:checkout.number/line_items/:id',
            },
          },
        },
        payment: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/payments',
            },
            create: {
              method: 'POST',
              path: '/api/checkouts/:checkout.number/payments',
            },
            new: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/payments/new',
            },
            edit: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/payments/:id/edit',
            },
            get: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/payments/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/checkouts/:checkout.number/payments/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/checkouts/:checkout.number/payments/:id',
            },
            authorize: {
              method: 'PUT',
              path: '/api/checkouts/:checkout.number/payments/:id/authorize',
            },
            capture: {
              method: 'PUT',
              path: '/api/checkouts/:checkout.number/payments/:id/capture',
            },
            purchase: {
              method: 'PUT',
              path: '/api/checkouts/:checkout.number/payments/:id/purchase',
            },
            void: {
              method: 'PUT',
              path: '/api/checkouts/:checkout.number/payments/:id/void',
            },
            credit: {
              method: 'PUT',
              path: '/api/checkouts/:checkout.number/payments/:id/credit',
            },
          },
        },
        address: {
          endpoints: {
            get: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/addresses/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/checkouts/:checkout.number/addresses/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/checkouts/:checkout.number/addresses/:id',
            },
          },
        },
        returnAuthorization: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/return_authorizations',
            },
            create: {
              method: 'POST',
              path: '/api/checkouts/:checkout.number/return_authorizations',
            },
            new: {
              method: 'GET',
              path: '/api/checkouts/:checkout.number/return_authorizations/new',
            },
            edit: {
              method: 'GET',
              path:
                '/api/checkouts/:checkout.number/return_authorizations/:id/edit',
            },
            cancel: {
              method: 'PUT',
              path:
                '/api/checkouts/:checkout.number/return_authorizations/:id/cancel',
            },
            update: {
              method: 'PATCH',
              path: '/api/checkouts/:checkout.number/return_authorizations/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/checkouts/:checkout.number/return_authorizations/:id',
            },
          },
        },
      },
    },

    variant: {
      endpoints: {
        get: {
          method: 'GET',
          path: '/api/variants',
        },
        create: {
          method: 'POST',
          path: '/api/variants',
        },
        new: {
          method: 'GET',
          path: '/api/variants/new',
        },
        edit: {
          method: 'GET',
          path: '/api/variants/:id/edit',
        },
        update: {
          method: 'PATCH',
          path: '/api/variants/:id',
        },
        delete: {
          method: 'GET',
          path: '/api/variants/:id',
        },
        image: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/variants/:variant.id/images',
            },
            create: {
              method: 'POST',
              path: '/api/variants/:variant.id/images',
            },
            new: {
              method: 'GET',
              path: '/api/variants/:variant.id/images/new',
            },
            edit: {
              method: 'GET',
              path: '/api/variants/:variant.id/images/:id/edit',
            },
            update: {
              method: 'PATCH',
              path: '/api/variants/:variant.id/images/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/variants/:variant.id/images/:id',
            },
          },
        },
      },
    },

    optionType: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/option_types',
        },
        create: {
          method: 'POST',
          path: '/api/option_types',
        },
        new: {
          method: 'GET',
          path: '/api/option_types/new',
        },
        edit: {
          method: 'GET',
          path: '/api/option_types/:id/edit',
        },
        get: {
          method: 'GET',
          path: '/api/option_types/:id',
        },
        update: {
          method: 'PATCH',
          path: '/api/option_types/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/option_types/:id',
        },

        optionValue: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/option_types/optionType.id/option_values',
            },
            create: {
              method: 'POST',
              path: '/api/option_types/optionType.id/option_values',
            },
            new: {
              method: 'GET',
              path: '/api/option_types/optionType.id/option_values/new',
            },
            edit: {
              method: 'GET',
              path: '/api/option_types/optionType.id/option_values/:id/edit',
            },
            get: {
              method: 'GET',
              path: '/api/option_types/optionType.id/option_values/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/option_types/optionType.id/option_values/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/option_types/optionType.id/option_values/:id',
            },
          },
        },
      },
    },

    optionValue: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/option_values',
        },
        create: {
          method: 'POST',
          path: '/api/option_values',
        },
        new: {
          method: 'GET',
          path: '/api/option_values/new',
        },
        edit: {
          method: 'GET',
          path: '/api/option_values/:id/edit',
        },
        get: {
          method: 'GET',
          path: '/api/option_values/:id',
        },
        update: {
          method: 'PATCH',
          path: '/api/option_values/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/option_values/:id',
        },
      },
    },

    order: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/orders',
        },
        create: {
          method: 'POST',
          path: '/api/orders',
        },
        new: {
          method: 'GET',
          path: '/api/orders/new',
        },
        edit: {
          method: 'GET',
          path: '/api/orders/:number/edit',
        },
        get: {
          method: 'GET',
          path: '/api/orders/:number',
        },
        update: {
          method: 'PATCH',
          path: '/api/orders/:number',
        },
        delete: {
          method: 'DELETE',
          path: '/api/orders/:number',
        },
        mine: {
          method: 'GET',
          path: '/api/orders/mine',
        },
        current: {
          method: 'GET',
          path: '/api/orders/current',
        },
        cancel: {
          method: 'PUT',
          path: '/api/orders/:number/cancel',
        },
        empty: {
          method: 'PUT',
          path: '/api/orders/:number/empty',
        },
        couponCode: {
          endpoints: {
            create: {
              method: 'POST',
              path: '/api/orders/:order.number/coupon_codes',
            },
            delete: {
              method: 'DELETE',
              path: '/api/orders/:order.number/coupon_codes/:id',
            },
          },
        },
        lineItem: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/orders/:order.number/line_items',
            },
            create: {
              method: 'POST',
              path: '/api/orders/:order.number/line_items',
            },
            new: {
              method: 'GET',
              path: '/api/orders/:order.number/line_items/new',
            },
            get: {
              method: 'GET',
              path: '/api/orders/:order.number/line_items/:id',
            },
            edit: {
              method: 'GET',
              path: '/api/orders/:order.number/line_items/:id/edit',
            },
            update: {
              method: 'PATCH',
              path: '/api/orders/:order.number/line_items/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/orders/:order.number/line_items/:id',
            },
          },
        },
        payment: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/orders/:order.number/payments',
            },
            create: {
              method: 'POST',
              path: '/api/orders/:order.number/payments',
            },
            new: {
              method: 'GET',
              path: '/api/orders/:order.number/payments/new',
            },
            edit: {
              method: 'GET',
              path: '/api/orders/:order.number/payments/:id/edit',
            },
            get: {
              method: 'GET',
              path: '/api/orders/:order.number/payments/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/orders/:order.number/payments/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/orders/:order.number/payments/:id',
            },
          },
        },
        address: {
          endpoints: {
            get: {
              method: 'GET',
              path: '/api/orders/:order.number/addresses/:id',
            },
            update: {
              method: 'PATCH',
              path: '/api/orders/:order.number/addresses/:id',
            },
          },
        },
        returnAuthorization: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/orders/:order.number/return_authorizations',
            },
            create: {
              method: 'POST',
              path: '/api/orders/:order.number/return_authorizations',
            },
            new: {
              method: 'GET',
              path: '/api/orders/:order.number/return_authorizations/new',
            },
            edit: {
              method: 'GET',
              path: '/api/orders/:order.number/return_authorizations/:id/edit',
            },
            cancel: {
              method: 'PUT',
              path:
                '/api/orders/:order.number/return_authorizations/:id/cancel',
            },
            update: {
              method: 'PATCH',
              path: '/api/orders/:order.number/return_authorizations/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/orders/:order.number/return_authorizations/:id',
            },
          },
        },
      },
    },

    zone: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/zones',
        },
        create: {
          method: 'POST',
          path: '/api/zones',
        },
        new: {
          method: 'GET',
          path: '/api/zones/new',
        },
        edit: {
          method: 'GET',
          path: '/api/zones/:id/edit',
        },
        get: {
          method: 'GET',
          path: '/api/zones/:id',
        },
        update: {
          method: 'PATCH',
          path: '/api/zones/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/zones/:id',
        },
      },
    },

    country: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/countries',
        },
        get: {
          method: 'GET',
          path: '/api/countries/:id',
        },
        state: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/countries/:country.id/states',
            },
            get: {
              method: 'GET',
              path: '/api/countries/:country.id/states/:id',
            },
          },
        },
      },
    },

    shipment: {
      endpoints: {
        transferToLocation: {
          method: 'POST',
          path: '/api/shipments/transfer_to_location',
        },

        transferToShipment: {
          method: 'POST',
          path: '/api/shipments/transfer_to_shipment',
        },

        mine: {
          method: 'GET',
          path: '/api/shipments/mine',
        },

        estimatedRates: {
          method: 'GET',
          path: '/api/shipments/:id/estimated_rates',
        },

        selectShippingMethod: {
          method: 'PUT',
          path: '/api/shipments/:id/select_shipping_method',
        },

        ready: {
          method: 'PUT',
          path: '/api/shipments/:id/ready',
        },

        ship: {
          method: 'PUT',
          path: '/api/shipments/:id/ship',
        },

        add: {
          method: 'PUT',
          path: '/api/shipments/:id/add',
        },

        remove: {
          method: 'PUT',
          path: '/api/shipments/:id/remove',
        },

        create: {
          method: 'POST',
          path: '/api/shipments',
        },

        update: {
          method: 'PATCH',
          path: '/api/shipments/:id',
        },
      },
    },

    taxonomy: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/taxonomies',
        },
        create: {
          method: 'POST',
          path: '/api/taxonomies',
        },
        new: {
          method: 'GET',
          path: '/api/taxonomies/new',
        },
        edit: {
          method: 'GET',
          path: '/api/taxonomies/:id/edit',
        },
        get: {
          method: 'GET',
          path: '/api/taxonomies/:id',
        },
        update: {
          method: 'PATCH',
          path: '/api/taxonomies/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/taxonomies/:id',
        },
        taxon: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/taxonomies/:taxonomy.id/taxons',
            },
            create: {
              method: 'POST',
              path: '/api/taxonomies/:taxonomy.id/taxons',
            },
            new: {
              method: 'GET',
              path: '/api/taxonomies/:taxonomy.id/taxons',
            },
            get: {
              method: 'GET',
              path: '/api/taxonomies/:taxonomy.id/taxons/:id',
            },
            edit: {
              method: 'GET',
              path: '/api/taxonomies/:taxonomy.id/taxons/:id/edit',
            },
            update: {
              method: 'PATCH',
              path: '/api/taxonomies/:taxonomy.id/taxons/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/taxonomies/:taxonomy.id/taxons/:id',
            },
          },
        },
      },
    },

    taxon: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/taxons',
        },
        product: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/taxons/products',
            },
          },
        },
      },
    },

    inventoryUnit: {
      endpoints: {
        get: {
          method: 'GET',
          path: '/api/inventory_units/:id',
        },
        update: {
          method: 'PATCH',
          path: '/api/inventory_units/:id',
        },
      },
    },

    user: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/users',
        },
        create: {
          method: 'POST',
          path: '/api/users',
        },
        new: {
          method: 'GET',
          path: '/api/users/new',
        },
        get: {
          method: 'GET',
          path: '/api/users/:id',
        },
        edit: {
          method: 'GET',
          path: '/api/users/:id/edit',
        },
        update: {
          method: 'PATCH',
          path: '/api/users/:id',
        },
        delete: {
          method: 'GET',
          path: '/api/users/:id',
        },
        creditCard: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/users/:user.id/credit_cards',
            },
          },
        },
        addressBook: {
          endpoints: {
            get: {
              method: 'GET',
              path: '/api/users/:user.id/address_book',
            },
            update: {
              method: 'PATCH',
              path: '/api/users/:user.id/address_book',
            },
            delete: {
              method: 'DELETE',
              path: '/api/users/:user.id/address_book',
            },
          },
        },
      },
    },

    creditCard: {
      endpoints: {
        update: {
          method: 'PATCH',
          path: '/api/credit_cards/:id',
        },
      },
    },

    property: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/properties',
        },
        create: {
          method: 'POST',
          path: '/api/properties',
        },
        new: {
          method: 'GET',
          path: '/api/properties/new',
        },
        get: {
          method: 'GET',
          path: '/api/properties/:id',
        },
        edit: {
          method: 'GET',
          path: '/api/properties/:id/edit',
        },
        update: {
          method: 'PATCH',
          path: '/api/properties/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/properties/:id',
        },
      },
    },

    stockLocation: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/stock_locations',
        },
        create: {
          method: 'POST',
          path: '/api/stock_locations',
        },
        new: {
          method: 'GET',
          path: '/api/stock_locations/new',
        },
        get: {
          method: 'GET',
          path: '/api/stock_locations/:id',
        },
        edit: {
          method: 'GET',
          path: '/api/stock_locations/:id/edit',
        },
        update: {
          method: 'PATCH',
          path: '/api/stock_locations/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/stock_locations/:id',
        },

        stockItem: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/stock_locations/:stockLocation.id/stock_items',
            },
            create: {
              method: 'POST',
              path: '/api/stock_locations/:stockLocation.id/stock_items',
            },
            new: {
              method: 'GET',
              path: '/api/stock_locations/:stockLocation.id/stock_items/new',
            },
            get: {
              method: 'GET',
              path: '/api/stock_locations/:stockLocation.id/stock_items/:id',
            },
            edit: {
              method: 'GET',
              path:
                '/api/stock_locations/:stockLocation.id/stock_items/:id/edit',
            },
            update: {
              method: 'PATCH',
              path: '/api/stock_locations/:stockLocation.id/stock_items/:id',
            },
            delete: {
              method: 'DELETE',
              path: '/api/stock_locations/:stockLocation.id/stock_items/:id',
            },
          },
        },

        stockMovement: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/stock_locations/:stockLocation.id/stock_movements',
            },
            create: {
              method: 'POST',
              path: '/api/stock_locations/:stockLocation.id/stock_movements',
            },
            new: {
              method: 'GET',
              path:
                '/api/stock_locations/:stockLocation.id/stock_movements/new',
            },
            get: {
              method: 'GET',
              path:
                '/api/stock_locations/:stockLocation.id/stock_movements/:id',
            },
            edit: {
              method: 'GET',
              path:
                '/api/stock_locations/:stockLocation.id/stock_movements/:id/edit',
            },
            update: {
              method: 'PATCH',
              path:
                '/api/stock_locations/:stockLocation.id/stock_movements/:id/update',
            },
            delete: {
              method: 'DELETE',
              path:
                '/api/stock_locations/:stockLocation.id/stock_movements/:id',
            },
          },
        },
      },
    },

    stockItem: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/stock_items',
        },
        update: {
          method: 'PATCH',
          path: '/api/stock_items/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/stock_items/:id',
        },
      },
    },

    store: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/stores',
        },
        create: {
          method: 'POST',
          path: '/api/stores',
        },
        new: {
          method: 'GET',
          path: '/api/stores/new',
        },
        get: {
          method: 'GET',
          path: '/api/stores/:id',
        },
        edit: {
          method: 'GET',
          path: '/api/stores/:id/edit',
        },
        update: {
          method: 'PATCH',
          path: '/api/stores/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/stores/:id',
        },
      },
    },

    storeCreditEvent: {
      endpoints: {
        mine: {
          method: 'GET',
          path: '/api/store_credit_events/mine',
        },
      },
    },

    config: {
      endpoints: {
        get: {
          method: 'GET',
          path: '/api/config',
        },
        money: {
          endpoints: {
            get: {
              method: 'GET',
              path: '/api/config/money',
            },
          },
        },
      },
    },

    classification: {
      endpoints: {
        update: {
          method: 'PUT',
          path: '/api/classifications',
        },
      },
    },
  },
})
